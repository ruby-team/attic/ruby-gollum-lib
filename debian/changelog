ruby-gollum-lib (4.2.7.10~gitlab.2+dfsg-1) unstable; urgency=medium

  * Fix watch file syntax
  * New upstream version 4.2.7.10~gitlab.2+dfsg
  * Remove patch uplied upstream

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Mar 2022 14:11:38 +0530

ruby-gollum-lib (4.2.7.10~gitlab.1+dfsg-3) unstable; urgency=medium

  * Team upload.
  * d/p/update-sanitize.patch: use sanitize 6.0
  * Bump debhelper compatibility level to 13
  * d/control: update ruby-sanitize version constraint

 -- Lucas Kanashiro <kanashiro@debian.org>  Fri, 28 Jan 2022 09:42:36 -0300

ruby-gollum-lib (4.2.7.10~gitlab.1+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Pirate Praveen ]
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Switch to gem-install layout for bundle --local compatibility

 -- Pirate Praveen <praveen@debian.org>  Mon, 24 Jan 2022 15:25:35 +0530

ruby-gollum-lib (4.2.7.10~gitlab.1+dfsg-1) unstable; urgency=medium

  * Team upload

  [ Pirate Praveen ]
  * Update watch file to use gitlab.com tags
  * New upstream version 4.2.7.10~gitlab.1
  * Bump minimum version of ruby-gollum-rugged-adaper

  [ Sruthi Chandran ]
  * Remove prebuilt windows binary for dfsg compliance

 -- Sruthi Chandran <srud@debian.org>  Thu, 24 Dec 2020 00:19:03 +0530

ruby-gollum-lib (4.2.7.9-3) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 01 Dec 2020 20:16:51 +0530

ruby-gollum-lib (4.2.7.9-2) experimental; urgency=medium

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * Update minimum version of sanitize to 5.2 (Closes: #974647)

 -- Pirate Praveen <praveen@debian.org>  Fri, 13 Nov 2020 17:32:50 +0530

ruby-gollum-lib (4.2.7.9-1) unstable; urgency=medium

  * Tighten dependency of ruby-gollum-rugged-adapter (for backports)
  * New upstream version 4.2.7.9

 -- Pirate Praveen <praveen@debian.org>  Sun, 24 May 2020 21:48:57 +0530

ruby-gollum-lib (4.2.7.8-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Pirate Praveen ]
  * New upstream version 4.2.7.8
  * Mention we are providing gitlab's fork of gollum-lib in description
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Remove patch to make rugged default adapter (fixed upstream)

 -- Pirate Praveen <praveen@debian.org>  Wed, 08 Apr 2020 12:29:07 +0530

ruby-gollum-lib (4.2.7.7-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Pirate Praveen ]
  * Reupload to unstable
  * Bump Standards-Version to 4.4.1 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12

 -- Pirate Praveen <praveen@debian.org>  Mon, 13 Jan 2020 21:49:16 +0530

ruby-gollum-lib (4.2.7.7-1) experimental; urgency=medium

  * Team upload
  * New upstream version 4.2.7.7
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Fix insecure URL

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Fri, 17 May 2019 16:01:44 +0530

ruby-gollum-lib (4.2.7.5-3) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.2.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Thu, 23 Aug 2018 15:57:19 +0530

ruby-gollum-lib (4.2.7.5-2) experimental; urgency=medium

  * Switch gollum-rugged_adapter dependency to gitlab fork

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jun 2018 17:44:28 +0530

ruby-gollum-lib (4.2.7.5-1) experimental; urgency=medium

  * Switch to gitlab fork as upstream
  * New upstream version 4.2.7.5

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jun 2018 16:29:59 +0530

ruby-gollum-lib (4.2.7-3) experimental; urgency=medium

  * Update sanitize dependency
  * Tighten ruby-sanitize dependency

 -- Pirate Praveen <praveen@debian.org>  Wed, 13 Jun 2018 19:42:21 +0530

ruby-gollum-lib (4.2.7-2) unstable; urgency=medium

  * Tighten dependency on ruby-rouge
  * Bump Standards-Version to 4.1.4 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Mon, 04 Jun 2018 20:59:04 +0530

ruby-gollum-lib (4.2.7-1) unstable; urgency=medium

  [ Pirate Praveen ]
  * New upstream version 4.2.7
  * Refresh/drop patches
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * Don't repack for extra gemspec, use debian/gemspec instead

 -- Pirate Praveen <praveen@debian.org>  Tue, 20 Mar 2018 17:40:45 +0530

ruby-gollum-lib (4.2.1+debian-1) unstable; urgency=medium

  * Remove gollum-lib_java.gemspec from orig.tar.gz (fix autopkgtest)
  * Use github-markup >= 1.5 (switch to gitlab-markup)

 -- Pirate Praveen <praveen@debian.org>  Mon, 31 Oct 2016 11:37:00 +0530

ruby-gollum-lib (4.2.1-1) unstable; urgency=medium

  * New upstream release
  * Remove rouge2.patch (already included upstream)

 -- Pirate Praveen <praveen@debian.org>  Thu, 14 Jul 2016 13:41:55 +0530

ruby-gollum-lib (4.2.0-3) unstable; urgency=medium

  * Team upload
  * Add rouge2.patch to adapt to ruby-rouge >= 2 (Closes: #830095)
  * Relax dependency on stringex in gemspec

 -- Cédric Boutillier <boutil@debian.org>  Sat, 09 Jul 2016 10:05:58 +0200

ruby-gollum-lib (4.2.0-2) unstable; urgency=medium

  [ Pirate Praveen ]
  * Update minimum version for ruby-rugged-adapter to 0.4.2
  * Update minimum version of ruby-github-markup to 1.4.0

  [ Cédric Boutillier ]

 -- Pirate Praveen <praveen@debian.org>  Sat, 04 Jun 2016 20:55:57 +0530

ruby-gollum-lib (4.2.0-1) unstable; urgency=medium

  * New upstream release (Closes: #821821)
  * Refresh patch
  * Bump standards version to 3.9.8 (no changes)

 -- Pirate Praveen <praveen@debian.org>  Thu, 02 Jun 2016 16:26:21 +0530

ruby-gollum-lib (4.1.0-3) unstable; urgency=medium

  * Make rugged_adapter default in gollum-lib.rb as well

 -- Pirate Praveen <praveen@debian.org>  Sat, 12 Dec 2015 14:07:58 +0530

ruby-gollum-lib (4.1.0-2) unstable; urgency=medium

  * Fix gollum-rugged_adapter version

 -- Pirate Praveen <praveen@debian.org>  Sat, 12 Dec 2015 13:43:23 +0530

ruby-gollum-lib (4.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #807334)

 -- Pirate Praveen <praveen@debian.org>  Mon, 07 Dec 2015 21:01:58 +0530
